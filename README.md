# lime-academy-main-task-week-1

Deployed ContractAddress: 0xaD8d1984b43d00ebb26a8aA960D3571eB7d1BB00

Contract has been verified.

Note: The hardhat task has been extracted to a separate file in /scripts/hardhat-tasks for better readability.

Test script calls:

npm run-script interact add-new-book --privateKey {PRIVATE_KEY} --title firstTestBook --author firstTestAuthor --copyCount 7

npm run-script interact borrow --privateKey {PRIVATE_KEY} --bookId {BOOK_ID}

npm run-script interact return --privateKey {PRIVATE_KEY} --bookId {BOOK_ID}

npm run-script interact get-all-available --privateKey {PRIVATE_KEY}

npm run-script interact add-copies --privateKey {PRIVATE_KEY} --bookId {BOOK_ID} --copies 3 

npm run-script interact get-book-borrower-history --privateKey {PRIVATE_KEY} --bookId {BOOK_ID}


