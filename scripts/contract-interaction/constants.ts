export const COMMANDS = {
    ADD_BOOK_COMMAND:  'add-new-book',
    ADD_COPIES_COMMAND: 'add-copies',
    BORROW_BOOK_COMMAND: 'borrow',
    RETURN_BOOK_COMMAND: 'return',
    GET_ALL_AVAILABLE_COMMAND: 'get-all-available',
    GET_BOOK__HISTORY_COMMAND : 'get-book-borrower-history',
} as const;

type CommandKey = keyof typeof COMMANDS;

export const COMMAND_ELEMENT_COUNT_DICT: Record<CommandKey, number> = {
    ADD_BOOK_COMMAND: 7,
    ADD_COPIES_COMMAND: 6,
    BORROW_BOOK_COMMAND: 5,
    RETURN_BOOK_COMMAND: 5,
    GET_ALL_AVAILABLE_COMMAND: 3,
    GET_BOOK__HISTORY_COMMAND: 5,
};


