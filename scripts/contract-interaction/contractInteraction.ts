import {BigNumber, ethers, Wallet} from 'ethers';
const config = require('../../config.json')
const library = require("../../artifacts/contracts/Library.sol/Library.json");
import secrets from '../../secrets.json';
import {MissingPrivateKeyException} from "../exceptions/MissingPrivateKeyException";

const contractAddress = config.PROJECT_SETTINGS.LIBRARY_CONTRACT_ADDRESS;
const provider = getProvider();

export async function createANewBook(title: String, author: string, copiesCount: BigNumber, privateKey: string) {
    const wallet = getWallet(privateKey);
    const libraryContract = getLibraryContract(wallet);

    const addBookTx = await libraryContract.addNewBook(title, author, copiesCount);
    addBookTx.wait();

    logTransactionOutput(addBookTx);
}

export async function addBookCopies(bookId: String, additionalCopiesCount: BigNumber, privateKey: string) {
    const wallet = getWallet(privateKey);
    const libraryContract = getLibraryContract(wallet);

    const addBookTx = await libraryContract.addCopiesToAnExistingBook(bookId, additionalCopiesCount)
    addBookTx.wait();

    logTransactionOutput(addBookTx)
}

export async function borrowBook(bookId: String, privateKey: string) {
    const wallet = getWallet(privateKey);
    const libraryContract = getLibraryContract(wallet);

    const borrowBookTx = await libraryContract.borrow(bookId);
    borrowBookTx.wait();

    logTransactionOutput(borrowBookTx);
}

export async function returnBook(bookId: String, privateKey: string) {
    const wallet = getWallet(privateKey);
    const libraryContract = getLibraryContract(wallet);

    const returnBookTx = await libraryContract.returnBook(bookId);
    returnBookTx.wait();

    logTransactionOutput(returnBookTx);
}

export async function getBookBorrowHistory(bookId: String, privateKey: string) {
    const wallet = getWallet(privateKey);
    const libraryContract = getLibraryContract(wallet);

    const getBookHistoryTx = await libraryContract.getBookBorrowers(bookId);

    logTransactionOutput(getBookHistoryTx);
}

export async function getAllAvailableBooks(privateKey: string) {
    const wallet = getWallet(privateKey);
    const libraryContract = getLibraryContract(wallet);

    const getAvailableTx = await libraryContract.getAllAvailableBooks();

    logTransactionOutput(getAvailableTx);
}

function getLibraryContract(wallet: Wallet) {
    return new ethers.Contract(
        contractAddress,
        library.abi,
        wallet
    );
}

function getUserPrivateKey(cliKeyInput: string) {
    if (cliKeyInput == null) {
        let secretsKey = secrets.PRIVATE_KEY;
        if (secretsKey == null) {
            throw new MissingPrivateKeyException('Wallet private key was not provided. Transaction will not execute.');
        }
        return secretsKey;
    }
    return cliKeyInput;
}

function getProvider() {
    if (config.PROJECT_SETTINGS.isLocal) {
        return new ethers.providers.JsonRpcProvider(
            "http://localhost:8545"
        );
    }
    return new ethers.providers.InfuraProvider(
        "sepolia",
        secrets.INFURA_KEY
    );
}

function getWallet (privateKey: string) {
    return new ethers.Wallet(
        getUserPrivateKey(privateKey),
        provider
    );
}

function logTransactionOutput(transaction: any) {
    console.info('Completed transaction data:');
    console.info(transaction);
}