const hre = require("hardhat")

export async function deployContract() {
    const libraryContractFactory = await hre.ethers.getContractFactory("Library");
    const library = await libraryContractFactory.deploy();
    await library.deployed();
    console.log(`The Library Contract was deployed to ${library.address}`);
}

deployContract().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});