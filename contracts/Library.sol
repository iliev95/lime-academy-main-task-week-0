// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.18;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Library is Ownable {
    bytes32[] public bookKey;
    mapping(bytes32 => Book) public books;
    mapping(bytes32 => SingleBorrowAction[]) public bookBorrowAction;
    mapping(address => mapping (bytes32 => bool)) public clientBook;

    struct Book {
        bytes32 bookId;
        string name;
        string author;
        uint8 availableCopiesCount;
    }

    // The struct is created for better visibility of who and when borrowed the book
    struct SingleBorrowAction {
        address borrowerAddress;
        uint timeStamp;
    }

    event BookAdded(bytes32 indexed bookId, string name, string author);
    event BookReturned(bytes32 indexed bookId, address borrower);
    event BookBorrowed(bytes32 indexed bookId, address borrower);
    event BookCopiesAdded(bytes32 indexed bookId, uint8 additionalCopyCount);

    modifier _isValidBook(string memory _bookTitle, string memory _author, uint _copyCount)  {
        bytes memory modTitle = bytes(_bookTitle);
        bytes memory modAuthor = bytes(_author);
        require(_copyCount > 0 && modAuthor.length > 0 && modTitle.length > 0, "Book input data is invalid");
        _;
    }

    modifier _bookExists(string memory _bookTitle, string memory _author)  {
        bytes32 newBookKey = _generateUniqueHashId(_bookTitle, _author);
        require(bytes(books[newBookKey].name).length == 0, "Book has already been added.");
        _;
    }

    function addNewBook (string memory _bookTitle, string memory _author, uint8 _copyCount) public onlyOwner _isValidBook(_bookTitle, _author, _copyCount) _bookExists(_bookTitle, _author) {
        bytes32 newBookId = _generateUniqueHashId(_bookTitle, _author);
        Book memory newBook = Book(newBookId, _bookTitle, _author, _copyCount);
        bookKey.push(newBookId);
        books[newBookId] = newBook;

        emit BookAdded(newBookId, _bookTitle, _author);
    }

    function addCopiesToAnExistingBook (bytes32 _bookId, uint8 _additionalCopyCount) public onlyOwner {
        Book storage book = books[_bookId];
        require (bytes(book.name).length > 0, "Book doesn't exist");
        book.availableCopiesCount += _additionalCopyCount;

        emit BookCopiesAdded(_bookId, _additionalCopyCount);
    }

    function borrow(bytes32 _bookId) public {
        uint timestamp = block.timestamp;
        address senderAddress = msg.sender;
        Book storage bookToBorrow = books[_bookId];
        require (bookToBorrow.availableCopiesCount > 0, "No available copies at the moment");
        require (_senderIsNotABorrower(bookToBorrow.bookId), "You already borrowed this book");

        clientBook[senderAddress][_bookId] = true;
        bookBorrowAction[_bookId].push(SingleBorrowAction(senderAddress, timestamp));
        bookToBorrow.availableCopiesCount--;

        emit BookBorrowed(_bookId, senderAddress);
    }

    function returnBook (bytes32 _bookId) public {
        address senderAddress = msg.sender;
        Book storage bookToBorrow = books[_bookId];
        require(!_senderIsNotABorrower(bookToBorrow.bookId), "You have not borrowed this book yet.");

        clientBook[senderAddress][_bookId] = false;
        bookToBorrow.availableCopiesCount++;

        emit BookReturned(_bookId, senderAddress);
    }

    function getBookBorrowers(bytes32 _bookId)  public view returns (SingleBorrowAction[] memory) {
        return bookBorrowAction[_bookId];
    }

    function getAllAvailableBooks() public view returns (Book[] memory) {
        uint8 counter = 0;
        uint bookKeyArrayLength = bookKey.length;

        for (uint i = 0; i < bookKeyArrayLength ; i++) {
            bytes32 currentBookKey = bookKey[i];
            Book memory currentBook = books[currentBookKey];
            if (currentBook.availableCopiesCount > 0) {
                counter++;
            }
        }

        Book[] memory availableBooks = new Book[](counter);
        uint8 avIndex = 0;
        for (uint i = 0; i < bookKeyArrayLength ; i++) {
            bytes32 currentBookKey = bookKey[i];
            Book memory currentBook = books[currentBookKey];
            if (currentBook.availableCopiesCount > 0) {
                availableBooks[avIndex] = currentBook;
                avIndex++;
            }
        }
        return availableBooks;
    }

    function _senderIsNotABorrower(bytes32 bookKeyArg) private view returns (bool) {
        return !clientBook[msg.sender][bookKeyArg];
    }

    function _generateUniqueHashId(string memory _title, string memory _authorName) private pure returns (bytes32) {
        string memory bookKeyInput = string.concat(_title, _authorName);
        return keccak256(abi.encodePacked(bookKeyInput));
    }
}