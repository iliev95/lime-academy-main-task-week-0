import { Library } from "./../typechain-types/contracts/Library";
import { expect,} from "chai";
import { ethers } from "hardhat";

const hre = require("hardhat")
require("@nomicfoundation/hardhat-chai-matchers")

describe("Library", function () {
    let libraryFactory;
    let library: Library;

    beforeEach(async function () {
        await hre.network.provider.send("hardhat_reset")
        libraryFactory = await ethers.getContractFactory("Library");
        // @ts-ignore
        library = await libraryFactory.deploy();
        await library.deployed();
    })

    it("Should add a new book when input is valid", async function () {
        const transaction = await library.addNewBook("Test Title", "Test Author", 3);
        await transaction.wait();

        await expect(transaction).emit(library, "BookAdded")

        const firstBookKey = await library.bookKey(0)
        const firstBookElement = await library.books(firstBookKey)

        expect(firstBookKey).to.not.be.null
        expect(firstBookElement).to.not.be.null
        expect(firstBookElement["bookId"]).to.equal(firstBookKey);
        expect(firstBookElement["name"]).to.equal("Test Title");
        expect(firstBookElement["author"]).to.equal("Test Author");
        expect(firstBookElement["availableCopiesCount"]).to.equal(3);
    });

    it("Should throw when book title is invalid ", async function () {
        await expect(library.addNewBook("", "Test Author", 3))
            .to.be.revertedWith("Book input data is invalid");
    });

    it("Should throw when book author is invalid ", async function () {
        await expect(library.addNewBook("Test Title", "", 3))
            .to.be.revertedWith("Book input data is invalid");
    });

    it("Should throw when copy count is invalid ", async function () {
        await expect(library.addNewBook("Test Title", "Test Author", 0))
            .to.be.revertedWith("Book input data is invalid");
    });

    it("Should throw when the book already exists", async function () {
        const transaction = await library.addNewBook("Test Title", "Test Author", 3);
        await transaction.wait();

        await expect(library.addNewBook("Test Title", "Test Author", 3))
            .to.be.revertedWith("Book has already been added.");
    });

    it("Should throw when external caller tries to add a book", async function () {
        const [owner, addr1] = await ethers.getSigners();
        await expect(library.connect(addr1).addNewBook("Test Title", "Test Author", 3))
            .to.be.revertedWith("Ownable: caller is not the owner")
    });

    it("Should borrow a book successfully", async function () {
        const [owner, addr1] = await ethers.getSigners();
        const addTransaction = await library.addNewBook("Test Title", "Test Author", 3);
        await addTransaction.wait();

        const bookKey = await library.bookKey(0);
        let borrowTx = await library.connect(addr1).borrow(bookKey)

        await expect(borrowTx).emit(library, 'BookBorrowed');

        let bookKeyPromise = await library.books(bookKey)
        let copiesCountAfterBorrow = bookKeyPromise["availableCopiesCount"]

        let borrowActionPromise = await library.bookBorrowAction(bookKey, 0);
        let borrowerAddress = borrowActionPromise["borrowerAddress"];

        await expect(copiesCountAfterBorrow).to.be.equal(2);
        await expect(borrowerAddress).to.be.equal(addr1.address);
    });

    it("Should throw when sender is current borrower", async function () {
        const addTransaction = await library.addNewBook("Test Title", "Test Author", 3);
        await addTransaction.wait();

        const bookKey = await library.bookKey(0);
        // Borrow the book first time
        await library.borrow(bookKey);

        // Assert there are no copies left after a second borrow
        await expect(library.borrow(bookKey)).to.be.revertedWith('You already borrowed this book')
    });

    it("Should throw when no more copies are currently available", async function () {
        const addTransaction = await library.addNewBook("Test Title", "Test Author", 1);
        await addTransaction.wait();

        const bookKey = await library.bookKey(0);
        // Borrow the book first time
        await library.borrow(bookKey);

        // Assert there are no copies left after a second borrow
        await expect(library.borrow(bookKey)).to.be.revertedWith('No available copies at the moment')
    });

    it("Should return a book successfully", async function () {
        const addTransaction = await library.addNewBook("Test Title", "Test Author", 3);
        await addTransaction.wait();

        const bookKey = await library.bookKey(0);
        await library.borrow(bookKey);
        let returnTx = await library.returnBook(bookKey)

        await expect(returnTx).emit(library, 'BookReturned');

        let bookKeyPromise = await library.books(bookKey)
        let copiesCountAfterBorrow = bookKeyPromise["availableCopiesCount"]

        await expect(copiesCountAfterBorrow).to.be.equal(3);
    });

    it("Should throw when trying to return a book, that wasn't borrowed", async function () {
        const addTransaction = await library.addNewBook("Test Title", "Test Author", 3);
        await addTransaction.wait();
        const randomAddress = '0x513626121ef8b24c9211770318cdfd28c0239c908deaa0f74e01dde146a4309d';

        await expect(library.returnBook(randomAddress)).to.be.revertedWith('You have not borrowed this book yet.')
    });

    it("Should return all book's borrow actions", async function () {
        const [owner, addr1] = await ethers.getSigners();
        await library.addNewBook("Test Title1", "Test Author1", 3);

        let lastBookKey = await library.bookKey(0);
        await library.borrow(lastBookKey);
        await library.connect(addr1).borrow(lastBookKey);
        let availableBooksArray = await library.getBookBorrowers(lastBookKey);

        expect(availableBooksArray).to.not.be.null;
        expect(availableBooksArray.length).equal(2);
    });

    it("Should return all available books", async function () {
        await library.addNewBook("Test Title1", "Test Author1", 3);
        await library.addNewBook("Test Title2", "Test Author2", 3);
        await library.addNewBook("Test Title3", "Test Author3", 1);

        let lastBookKey = await library.bookKey(2);
        await library.borrow(lastBookKey);
        let availableBooksArray = await library.getAllAvailableBooks();

        expect(availableBooksArray).to.not.be.null;
        expect(availableBooksArray.length).equal(2);
    });

    it("Should add more copies to an existing book", async function () {
        await library.addNewBook("Test Title1", "Test Author1", 3);

        let lastBookKey = await library.bookKey(0);

        const addCopiesTx = await library.addCopiesToAnExistingBook(lastBookKey, 7);
        await addCopiesTx.wait();

        let availableBooksArray = await library.getAllAvailableBooks();

        expect(availableBooksArray).to.not.be.null;
        expect(availableBooksArray[0].availableCopiesCount).equal(10);
    });

    it("Should throw when external caller tries to add copies to a book ", async function () {
        const [owner, addr1] = await ethers.getSigners();
        const addTransaction = await library.addNewBook("Test Title", "Test Author", 3);
        await addTransaction.wait();

        let lastBookKey = await library.bookKey(0);
        await expect(library.connect(addr1).addCopiesToAnExistingBook(lastBookKey, 7)).to.be.revertedWith("Ownable: caller is not the owner")
    });

    it("Should throw when trying to add a book, which doesn't exist", async function () {
        const testBookKey = '0xd86fbbaac63b66bbefa9d81612f0e6626b6938a9f01083fcc2a121beb3c1f78E';
        await library.addNewBook("Test Title1", "Test Author1", 3);

        await expect(library.addCopiesToAnExistingBook(testBookKey, 7)).to.be.revertedWith("Book doesn't exist")
    });
});